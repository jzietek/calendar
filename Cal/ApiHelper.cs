﻿using System.Net.Http;

namespace Cal
{
    /// <summary>
    /// This class helps to use Api
    /// </summary>
    public static class ApiHelper
    {
        /// <summary>
        /// Base object that makes possible sending/receiving the HTTP requests/responses from a URL
        /// </summary>
        public static HttpClient ApiClient { get; set; }

        /// <summary>
        /// Method that initialize client.
        /// </summary>
        public static void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            //Give the requst as a json
            ApiClient.DefaultRequestHeaders.Accept.Add
                (new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
