﻿using System.Collections.Generic;

namespace Cal
{
    /// <summary>
    /// Class that stores info about weather.
    /// </summary>
    class WeatherInfo
    {
        /// <summary>
        /// Class conncted with api - weather.
        /// </summary>
        public class weather
        {
            public string main { get; set; }
            public string description { get; set; }
            public string icon { get; set; }
        }

        /// <summary>
        /// Class conncted with api - main.
        /// </summary>
        public class main
        {
            public double temp { get; set; }
            public double pressure { get; set; }
            public double humidity { get; set; }
        }

        /// <summary>
        /// Class conncted with api - wind.
        /// </summary>
        public class wind
        {
            public double speed { get; set; }
            public double deg { get; set; }
        }

        /// <summary>
        /// Class conncted with api - sys.
        /// </summary>
        public class sys
        {
            public long sunrise { get; set; }
            public long sunset { get; set; }
        }

        /// <summary>
        /// Class conncted with api - root. 
        /// </summary>
        public class root
        {
            public List<weather> weather { get; set; }
            public main main { get; set; }
            public sys sys { get; set; }
            public wind wind { get; set; }
            public double timezone { get; set; }
            public double visibility { get; set; }
        }
    }
}
