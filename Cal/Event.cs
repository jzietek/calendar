﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace Cal
{
    /// <summary>
    /// An event's model.
    /// </summary>
    public class Event
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Event's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Event's description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Event's beggining.
        /// </summary>
        public TimeSpan Beggining { get; set; }

        /// <summary>
        /// Event's end.
        /// </summary>
        public TimeSpan End { get; set; }

        /// <summary>
        /// Information about a progress of the event.
        /// </summary>
        public bool Done { get; set; }

        /// <summary>
        /// A foreign key that give possibility to connect event with a specific day.
        /// </summary>
        [ForeignKey("Day")]
        public DateTime DayId { get; set; }

        /// <summary>
        /// A virtual day.
        /// </summary>
        public virtual Day Day { get; set; }
    }
}
