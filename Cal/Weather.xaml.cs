﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Cal
{
    /// <summary>
    /// Weather class.
    /// </summary>
    public partial class Weather : Window
    {
        /// <summary>
        /// A constructor that initializes a view.
        /// </summary>
        public Weather()
        {
            InitializeComponent();

        }

        /// <summary>
        /// Enables draging window with left mouse click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Changes degrees to direction of the wind.
        /// </summary>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public static string DegreesToCardinalDetailed(double degrees)
        {
            string[] caridnals = { "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N" };
            var test = caridnals[(int)Math.Round(((double)degrees * 10 % 3600) / 225)];
            return test.ToString();
        }

        /// <summary>
        /// Sets initial city as Wrocław.
        /// </summary>
        public string City { get; set; } = "Wrocław";

        /// <summary>
        /// Loades weather info about a city in real time.
        /// </summary>
        /// <param name="City">Given city.</param>
        /// <returns></returns>
        private async Task LoadInfo(string City)
        {
            var weather = await WeatherProcessor.LoadWeather(City);

            ActualCity.Text = City.ToString();
            ImageSource imageSource = new ImageSourceConverter().ConvertFromString($"https://openweathermap.org/img/w/{weather.weather[0].icon}.png") as ImageSource;
            WeatherIcon.Source = imageSource;
            WeatherTemperature.Text = (Math.Round((weather.main.temp - 273.15) * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "°C ";
            WeatherSkyDescription.Text = weather.weather[0].description;
            WeatherSunrise.Text = convertDateTime(weather.sys.sunrise).ToShortTimeString();
            WeatherSunset.Text = convertDateTime(weather.sys.sunset).ToShortTimeString();
            WeatherPressure.Text = weather.main.pressure.ToString();
            WeatherWindSpeed.Text = weather.wind.speed.ToString();
            WeatherHumidity.Text = weather.main.humidity.ToString();
            double TimeDiff = weather.timezone / 3600 - 2;
            DateTime today = DateTime.Now;
            WeatherDayTime.Text = DateTime.Today.DayOfWeek.ToString() + ", " + today.AddHours(TimeDiff).ToString("HH/mm");
            WeatherVisibility.Text = (weather.visibility / 1000).ToString();
            WeatherWindDirection.Text = DegreesToCardinalDetailed(weather.wind.deg);
        }

        /// <summary>
        /// Converts time.
        /// </summary>
        /// <param name="millisec"></param>
        /// <returns></returns>
        DateTime convertDateTime(long millisec)
        {
            DateTime day = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).ToLocalTime();
            day = day.AddSeconds(millisec).ToLocalTime();

            return day;
        }

        /// <summary>
        /// Happens when windows is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await LoadInfo(City);
        }

        /// <summary>
        /// Change city, when button is clicked and city exist, then loads info again.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ChangeCityButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadInfo(textSearch.Text);
            }

            catch (Exception myException)
            {
                MessageBox.Show(myException.Message + "  Incorrect city name!");

            }
            textSearch.Text = "";
        }
    }
}
