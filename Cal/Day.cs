﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace Cal
{
    /// <summary>
    /// A day's model.
    /// </summary>
    public class Day
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        /// <summary>
        /// Primary key - date.
        /// </summary>
        public DateTime Id { get; set; }

        /// <summary>
        /// Info about a mood in a specific day.
        /// </summary>
        public int Mood { get; set; }
    }
}
