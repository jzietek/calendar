﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Cal
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Below objects are used in week's overview.
        /// </summary>
        public TextBlock tbDay = new TextBlock();
        public CheckBox checkBox = new CheckBox();
        public DateTime day1;
        public DateTime day2;
        public DateTime day3;
        public DateTime day4;
        public DateTime day5;

        /// <summary>
        /// Initializing everything on main window.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
            YourWeekView_Initialized();
        }

        /// <summary>
        /// Change view of the calendar to decade view when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DecadeButton_Click(object sender, RoutedEventArgs e)
        {
            MyCalendar.DisplayMode = CalendarMode.Decade;
        }

        /// <summary>
        /// Change view of the calendar to year view when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void YearButton_Click(object sender, RoutedEventArgs e)
        {
            MyCalendar.DisplayMode = CalendarMode.Year;
        }

        /// <summary>
        /// Change view of the calendar to month view when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MonthButton_Click(object sender, RoutedEventArgs e)
        {
            MyCalendar.DisplayMode = CalendarMode.Month;
        }

        /// <summary>
        /// Activates a weather window when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WeatherButton_Click(object sender, RoutedEventArgs e)
        {
            Weather weather = new Weather();
            weather.Owner = this;  // new window on top
            weather.Show();
        }

        /// <summary>
        /// Adds new day.
        /// </summary>
        /// <param name="day">Added day</param>
        /// <param name="date">Date of the added day.</param>
        public void AddNewDay(Day day, DateTime date)
        {
            using (var context = new CalendarContext())
            {
                day.Id = date;
                context.Dates.Add(day);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Adds new event.
        /// </summary>
        /// <param name="ev">Added event.</param>
        /// <param name="date">Date of the day when event takes place.</param>
        /// <param name="beg">Event's beginning time.</param>
        /// <param name="end">Event's end time.</param>
        /// <param name="name">Event's title.</param>
        /// <param name="desription">Event's description.</param>
        /// <param name="done">Info about progress.</param>
        public void AddNewEvent(Event ev, DateTime date, TimeSpan beg, TimeSpan end, string name,
            string desription = "", bool done = false)
        {
            using (var context = new CalendarContext())
            {
                var day = context.Dates.Where(s => s.Id == date).FirstOrDefault();

                ev.Beggining = beg;
                ev.End = end;
                ev.Name = name;
                ev.Description = desription;
                ev.Done = done;
                ev.DayId = day.Id;
                context.Events.Add(ev);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes event.
        /// </summary>
        /// <param name="ev">Deleted event.</param>
        public void DeleteEvent(Event ev)
        {
            using (var context = new CalendarContext())
            {
                var temp = context.Events.Single(s => s.Id == ev.Id);
                context.Events.Remove(temp);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Changes status of the event.
        /// </summary>
        /// <param name="ev">An event that's status is to be changed.</param>
        public void ChangeEventStatus(Event ev)
        {
            using (var context = new CalendarContext())
            {
                var temp = context.Events.Single(s => s.Id == ev.Id);
                context.Entry(temp).Entity.Done = !temp.Done;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Changes mood rate in selected day.
        /// </summary>
        /// <param name="day">A day that's mood is to be changed.</param>
        /// <param name="mood">New mood.</param>
        public void ChangeMoodRate(Day day, int mood)
        {
            using (var context = new CalendarContext())
            {
                var temp = context.Dates.Single(s => s.Id == day.Id);
                context.Entry(temp).Entity.Mood = mood;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Opens timetable view of specific day when button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = new CalendarContext())
            {
                Day day = new Day
                {
                    Id = MyCalendar.SelectedDate.Value
                };

                if (!context.Dates.Any(d => d.Id == day.Id))
                {
                    AddNewDay(day, day.Id);
                }

            }
            Timetable TT = new Timetable();
            TT.Owner = this;  // new window on top
            TT.ResizeMode = ResizeMode.CanResize;
            TT.Show();
            TT.Title = MyCalendar.SelectedDate.Value.Date.ToShortDateString();
        }

        /// <summary>
        /// Writes tasks to list box.
        /// </summary>
        /// <param name="listBox">Events are to be written here.</param>
        /// <param name="date">Right date.</param>
        /// <param name="isDone">Info about progress used to write event on correct side.</param>
        public void WriteTasksToListBox(ListBox listBox, DateTime date, bool isDone)
        {
            using (var context = new CalendarContext())
            {
                IQueryable query;
                if (isDone == true)
                {
                    query = context.Events.Where(s => (s.DayId == date) && (s.Done == true));
                }
                else
                {
                    query = context.Events.Where(s => (s.DayId == date) && (s.Done == false));
                }
                foreach (Event ev in query)
                {
                    ListBoxItem item = new ListBoxItem
                    {
                        Content = ev.Name,
                        Name = "ev" + ev.Id
                    };
                    if (isDone == true)
                    {
                        item.Foreground = Brushes.Green;
                    }
                    else
                    {
                        item.Foreground = Brushes.Red;
                    }
                    listBox.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// Writes tasks to stack panel.
        /// </summary>
        /// <param name="stackPanel">Events are to be written here.</param>
        /// <param name="date">Right date.</param>
        /// <param name="done">Info about progress.</param>
        public void WriteTasksToStackPanel(StackPanel stackPanel, DateTime date, bool done)
        {
            using (var context = new CalendarContext())
            {
                var query = context.Events.Where(s => (s.DayId == date) && (s.Done == done));
                foreach (Event ev in query)
                {
                    TextBlock temp = new TextBlock
                    {
                        Text = ev.Name,
                    };
                    stackPanel.Children.Add(temp);
                }
            }
        }

        /// <summary>
        /// Reloades tasks in list box.
        /// </summary>
        /// <param name="listBox">A list box that is reloaded</param>
        /// <param name="date">Right date.</param>
        /// <param name="isDone">Info about progress.</param>
        public void ReloadTasksInListBox(ListBox listBox, DateTime date, bool isDone)
        {
            while (listBox.Items.Count > 0)
            {
                listBox.Items.RemoveAt(listBox.Items.Count - 1);
            }
            WriteTasksToListBox(listBox, date, isDone);
        }

        /// <summary>
        /// Reloades a stack panel.
        /// </summary>
        /// <param name="stackPanel">A stack panel that is going to be reloaded.</param>
        /// <param name="date">Right date.</param>
        /// <param name="done">Info about progress.</param>
        public void ReloadStackPanel(StackPanel stackPanel, DateTime date, bool done)
        {
            using (var context = new CalendarContext())
            {
                {
                    while (stackPanel.Children.Count > 0)
                    {
                        stackPanel.Children.RemoveAt(stackPanel.Children.Count - 1);
                    }
                    WriteTasksToStackPanel(stackPanel, date, done);
                }
            }
        }

        /// <summary>
        /// Initializes your week's overview.
        /// </summary>
        private void YourWeekView_Initialized()
        {
            day1 = DateTime.Today.AddDays(-1);
            day2 = DateTime.Today;
            day3 = DateTime.Today.AddDays(1);
            day4 = DateTime.Today.AddDays(2);
            day5 = DateTime.Today.AddDays(3);

            tbDayC1.Text = "Yesterday";
            tbDayC2.Text = "Today";
            tbDayC3.Text = "Tomorrow";
            tbDayC4.Text = day4.Date.ToString("dd/MM/yyyy");
            tbDayC5.Text = day5.Date.ToString("dd/MM/yyyy");

            WriteTasksToStackPanel(spB1, day1, true);
            WriteTasksToStackPanel(spB2, day2, false);
            WriteTasksToStackPanel(spB3, day3, false);
            WriteTasksToStackPanel(spB4, day4, false);
            WriteTasksToStackPanel(spB5, day5, false);

            LoadMoodWeekView(B1TBRate, day1);
            LoadMoodWeekView(B2TBRate, day2);
            LoadMoodWeekView(B3TBRate, day3);
            LoadMoodWeekView(B4TBRate, day4);
            LoadMoodWeekView(B5TBRate, day5);

        }

        /// <summary>
        /// Loades mood week view.
        /// </summary>
        /// <param name="textBlock">A textblock where mood is to be written.</param>
        /// <param name="dateTime">Right date.</param>
        private void LoadMoodWeekView(TextBlock textBlock, DateTime dateTime)
        {

            using (var context = new CalendarContext())
            {
                var query = context.Dates.Where(s => s.Id == dateTime).FirstOrDefault();

                if (query != null)
                {
                    if (query.Mood != 0)
                    {
                        textBlock.Text = " Your mood in this day: " + query.Mood;
                    }
                    else
                    {
                        textBlock.Text = "Not rate your mood yet";
                    }
                }
                else
                {
                    textBlock.Text = "Not rate your mood yet";
                }
            }
        }

        /// <summary>
        /// Reloades whole week view.
        /// </summary>
        /// <param name="date">Right date.</param>
        public void ReloadYourWeekView(DateTime date)
        {

            if (MyCalendar.SelectedDate.Value >= day1 && MyCalendar.SelectedDate.Value <= day5)
            {

                if (MyCalendar.SelectedDate.Value == day1)
                {
                    ReloadStackPanel(spB1, day1, true);
                    LoadMoodWeekView(B1TBRate, day1);
                }
                else if (MyCalendar.SelectedDate.Value == day2)
                {
                    ReloadStackPanel(spB2, day2, false);
                    LoadMoodWeekView(B2TBRate, day2);
                }
                else if (MyCalendar.SelectedDate.Value == day3)
                {
                    ReloadStackPanel(spB3, day3, false);
                    LoadMoodWeekView(B3TBRate, day3);
                }
                else if (MyCalendar.SelectedDate.Value == day4)
                {
                    ReloadStackPanel(spB4, day4, false);
                    LoadMoodWeekView(B4TBRate, day4);
                }
                else
                {
                    ReloadStackPanel(spB5, day5, false);
                    LoadMoodWeekView(B5TBRate, day5);
                }
            }
        }

        /// <summary>
        /// Checks which option was chosen and shows it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ComboBoxStats.SelectedItem != null && DataPickerStats.SelectedDate != null)
            {
                if (ComboBoxStats.SelectedIndex == 0)
                {
                    SpStats.Visibility = Visibility.Collapsed;
                    SpBarGraph.Visibility = Visibility.Visible;
                    LoadBarGraph();
                }
                else
                {
                    SpBarGraph.Visibility = Visibility.Collapsed;
                    SpStats.Visibility = Visibility.Visible;
                    LoadPieChart();
                }
            }
            else
            {
                Error er = new Error();
                er.ErrorWindow.Text = "Error: Type of stats or date not selected";
                er.Show();
            }
        }

        /// <summary>
        /// Loads bar graph with stats of rate mood in every day in chosen month.
        /// </summary>
        public void LoadBarGraph()
        {
            double AverageMood = 0;
            int counter = 0;
            while (SpBarGraph.Children.Count > 2)
            {
                SpBarGraph.Children.RemoveAt(SpBarGraph.Children.Count - 1);
            }


            using (var context = new CalendarContext())
            {
                List<Item> Items = new List<Item>();
                DateTime tempDateTime = DataPickerStats.SelectedDate.Value;
                TbBarGraph.Text = "Rates of the mood in particular days in " + DataPickerStats.SelectedDate.Value.Month + "." + DataPickerStats.SelectedDate.Value.Year;
                while (tempDateTime.Month == DataPickerStats.SelectedDate.Value.Month)
                {
                    var query = context.Dates.Where(s => s.Id == tempDateTime).FirstOrDefault();

                    if (query != null)
                    {
                        Item temp = new Item()
                        {
                            Day = tempDateTime.Day.ToString(),
                            Mood = query.Mood,
                        };
                        Items.Add(temp);
                        if (query.Mood != 0)
                        {
                            counter++;
                            AverageMood += query.Mood;
                        }
                    }
                    else
                    {
                        Item temp = new Item()
                        {
                            Day = tempDateTime.Day.ToString(),
                            Mood = 0,
                        };
                        Items.Add(temp);
                    }
                    tempDateTime = tempDateTime.AddDays(1);
                }
                if (counter != 0)
                {
                    AverageMood = AverageMood / counter;
                    TbBarGraphAverage.Text = " Average mood in this month: " + Math.Round(AverageMood, 2);
                }
                else
                {
                    TbBarGraphAverage.Text = " Average mood in this month: 0";
                }
                DrawBarGraph(Items);
            }
        }

        /// <summary>
        /// Draws bar graph with stats of rate mood in every day in chosen month
        /// </summary>
        /// <param name="Items">Items used to draw a bar graph.</param>
        public void DrawBarGraph(List<Item> Items)
        {

            Canvas canvas = new Canvas();
            float chartWidth = 800, chartHeight = 300, axisMargin = 50, yAxisInterval = 20,
            blockWidth = 10, blockMargin = 12;
            canvas.Width = chartWidth;
            canvas.Height = chartHeight;

            Point yAxisEndPoint = new Point(axisMargin, axisMargin);
            Point origin = new Point(axisMargin, chartHeight - axisMargin);
            Point xAxisEndPoint = new Point(chartWidth - axisMargin, chartHeight - axisMargin);

            double yValue = 0;
            var yAxisValue = origin.Y;
            while (yAxisValue >= yAxisEndPoint.Y)
            {
                Line yLine = new Line()
                {
                    Stroke = Brushes.Gray,
                    StrokeThickness = 1,
                    X1 = origin.X,
                    Y1 = yAxisValue,
                    X2 = xAxisEndPoint.X,
                    Y2 = yAxisValue,
                };
                canvas.Children.Add(yLine);

                TextBlock yAxisTextBlock = new TextBlock()
                {
                    Text = $"{yValue / 20}",
                    Foreground = Brushes.Black,
                    FontSize = 16,
                };
                canvas.Children.Add(yAxisTextBlock);

                Canvas.SetLeft(yAxisTextBlock, origin.X - 35);
                Canvas.SetTop(yAxisTextBlock, yAxisValue - 12.5);

                yAxisValue -= yAxisInterval;
                yValue += yAxisInterval;
            }

            var margin = origin.X + blockMargin;
            foreach (var item in Items)
            {
                Rectangle block = new Rectangle()
                {
                    Fill = new SolidColorBrush(Color.FromRgb(68, 114, 196)),
                    Width = blockWidth,
                    Height = item.Mood * 20,
                };

                canvas.Children.Add(block);
                Canvas.SetLeft(block, margin);
                Canvas.SetTop(block, origin.Y - block.Height);

                TextBlock blockHeader = new TextBlock()
                {
                    Text = item.Day,
                    FontSize = 16,
                    Foreground = Brushes.Black,

                };
                canvas.Children.Add(blockHeader);
                Canvas.SetLeft(blockHeader, margin - 0.5);
                Canvas.SetTop(blockHeader, origin.Y + 5);

                margin += (blockWidth + blockMargin);
            }
            SpBarGraph.Children.Add(canvas);
        }

        /// <summary>
        /// Loads pie chart with stats of done and undone tasks.
        /// </summary>
        public void LoadPieChart()
        {
            while (SpStats.Children.Count > 1)
            {
                SpStats.Children.RemoveAt(SpStats.Children.Count - 1);
            }

            using (var context = new CalendarContext())
            {
                var done = context.Events.Where(s => (s.DayId.Year == DataPickerStats.SelectedDate.Value.Year) && (s.DayId.Month == DataPickerStats.SelectedDate.Value.Month) && (s.Done == true));
                var unDone = context.Events.Where(s => (s.DayId.Year == DataPickerStats.SelectedDate.Value.Year) && (s.DayId.Month == DataPickerStats.SelectedDate.Value.Month) && (s.Done == false));

                List<Tasks> tasks = new List<Tasks>()
                        {
                            new Tasks
                            {
                                Title = "Done Tasks",
                                Percentage = Math.Round((((double)(done.Count() * 100) / (double)(unDone.Count() + done.Count()))), 2),
                                ColorBrush = Brushes.Green,
                            },

                            new Tasks
                            {
                                Title = "Undone Tasks",
                                Percentage = Math.Round((((double)(unDone.Count() * 100) / (double)(unDone.Count() + done.Count()))), 2),
                                ColorBrush = Brushes.Red,
                            },
                        };
                if (done.Count() + unDone.Count() < 1)
                {
                    Error er = new Error();
                    er.ErrorWindow.Text = "Error: There is no tasks in this month";
                    er.Show();

                    ComboBoxStats.Text = "";
                    DataPickerStats.Text = "";

                    SpStats.Visibility = Visibility.Collapsed;

                }
                else
                {
                    DrawPieChart(tasks);
                }
            }
        }

        /// <summary>
        /// Reloads pie chart with stats of done and undone tasks.
        /// </summary>
        public void ReloadPieChart()
        {
            while (SpStats.Children.Count > 1)
            {
                SpStats.Children.RemoveAt(SpStats.Children.Count - 1);
            }

            using (var context = new CalendarContext())
            {
                var done = context.Events.Where(s => (s.DayId.Year == DataPickerStats.SelectedDate.Value.Year) && (s.DayId.Month == DataPickerStats.SelectedDate.Value.Month) && (s.Done == true));
                var unDone = context.Events.Where(s => (s.DayId.Year == DataPickerStats.SelectedDate.Value.Year) && (s.DayId.Month == DataPickerStats.SelectedDate.Value.Month) && (s.Done == false));

                List<Tasks> tasks = new List<Tasks>()
                        {

                            new Tasks
                            {
                                Title = "Done Tasks",
                                Percentage = Math.Round((((double)(done.Count() * 100) / (double)(unDone.Count() + done.Count()))), 2),
                                ColorBrush = Brushes.Green,
                            },

                            new Tasks
                            {
                                Title = "Undone Tasks",
                                Percentage = Math.Round((((double)(unDone.Count() * 100) / (double)(unDone.Count() + done.Count()))), 2),
                                ColorBrush = Brushes.Red,
                            },
                        };
                if (done.Count() + unDone.Count() < 1)
                {
                    ComboBoxStats.Text = "";
                    DataPickerStats.Text = "";

                    SpStats.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DrawPieChart(tasks);
                }
            }
        }

        /// <summary>
        /// Draws pie chart with stats of done and undone tasks
        /// </summary>
        /// <param name="tasks">Used to draw pie chart.</param>
        private void DrawPieChart(List<Tasks> tasks)
        {

            Canvas canvas = new Canvas();
            float pieWidth = 250, pieHeight = 250, centerX = pieWidth / 2, centerY = pieHeight / 2, radius = pieWidth / 2;
            canvas.Width = pieWidth;
            canvas.Height = pieHeight;
            canvas.Margin = new Thickness(50, 0, 0, 0);

            detailsItemsControl.ItemsSource = tasks;

            double angle = 0, prevAngle = 0;
            foreach (var task in tasks)
            {
                double line1X = (radius * Math.Cos(angle * Math.PI / 180)) + centerX;
                double line1Y = (radius * Math.Sin(angle * Math.PI / 180)) + centerY;

                if (task.Percentage == 100)
                {
                    angle = 359;
                }
                else
                {
                    angle = task.Percentage * (double)360 / 100 + prevAngle;
                }

                double arcX = (radius * Math.Cos(angle * Math.PI / 180)) + centerX;
                double arcY = (radius * Math.Sin(angle * Math.PI / 180)) + centerY;

                var line1Segment = new LineSegment(new Point(line1X, line1Y), false);
                double arcWidth = radius, arcHeight = radius;
                bool isLargeArc = task.Percentage > 50;
                var arcSegment = new ArcSegment()
                {
                    Size = new Size(arcWidth, arcHeight),
                    Point = new Point(arcX, arcY),
                    SweepDirection = SweepDirection.Clockwise,
                    IsLargeArc = isLargeArc,
                };
                var line2Segment = new LineSegment(new Point(centerX, centerY), false);

                var pathFigure = new PathFigure(
                    new Point(centerX, centerY),
                    new List<PathSegment>()
                    {
                        line1Segment,
                        arcSegment,
                        line2Segment,
                    },
                    true);

                var pathFigures = new List<PathFigure>() { pathFigure, };
                var pathGeometry = new PathGeometry(pathFigures);
                var path = new Path()
                {
                    Fill = task.ColorBrush,
                    Data = pathGeometry,
                };
                canvas.Children.Add(path);

                prevAngle = angle;


                // draw outlines
                var outline1 = new Line()
                {
                    X1 = centerX,
                    Y1 = centerY,
                    X2 = line1Segment.Point.X,
                    Y2 = line1Segment.Point.Y,
                    Stroke = Brushes.White,
                    StrokeThickness = 5,
                };
                var outline2 = new Line()
                {
                    X1 = centerX,
                    Y1 = centerY,
                    X2 = arcSegment.Point.X,
                    Y2 = arcSegment.Point.Y,
                    Stroke = Brushes.White,
                    StrokeThickness = 5,
                };

                canvas.Children.Add(outline1);
                canvas.Children.Add(outline2);
            }

            SpStats.Children.Add(canvas);
        }
    }
    /// <summary>
    /// Class which object are used to generate a Pie Chart
    /// </summary>
    public class Tasks
    {
        public string Title { get; set; }
        public double Percentage { get; set; }
        public Brush ColorBrush { get; set; }

    }
    /// <summary>
    /// Class which object are used to generate a Bar Graph
    /// </summary>
    public class Item
    {
        public string Day { get; set; }
        public int Mood { get; set; }
    }
}
