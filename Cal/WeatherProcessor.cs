﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cal
{
    /// <summary>
    /// Class responsible for connecting with url.
    /// </summary>
    class WeatherProcessor
    {
        /// <summary>
        /// Api key.
        /// </summary>
        static string ApiKey = "dd2f8405d68e30fb815033d7b4ffdde8";

        /// <summary>
        /// Connects with url.
        /// </summary>
        /// <param name="City"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static async Task<WeatherInfo.root> LoadWeather(string City)
        {
            string url = string.Format("https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}", City, ApiKey);

            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    WeatherInfo.root weather = await response.Content.ReadAsAsync<WeatherInfo.root>();

                    return weather;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
