using System.Data.Entity;

namespace Cal
{
    /// <summary>
    /// Calendar Context creates a bridge between an app and a database.
    /// </summary>
    public class CalendarContext : DbContext
    {
        // Your context has been configured to use a 'CalendarContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'CalendarClass.CalendarContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CalendarContext' 
        // connection string in the application configuration file.

        /// <summary>
        /// A constructor.
        /// </summary>
        public CalendarContext()
            : base("name=CalendarContext")
        {
        }

        /// <summary>
        /// A description of a days table.
        /// </summary>
        public DbSet<Day> Dates { get; set; }

        /// <summary>
        /// A description of a events table.
        /// </summary>
        public DbSet<Event> Events { get; set; }
    }
}